<?php
namespace Reed\EcommAtlasIntegration\Synch\Sale\Observer;

use GuzzleHttp\Psr7\Request;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Reed\EcommAtlasIntegration\Synch\Sale\OrderPublisher;
use Reed\EcommAtlasIntegration\Synch\Sale\DataTransfer;


class OrderPlaceAfter implements ObserverInterface
{
    protected $_logger;
    protected $_publisher;

    /**
     * @param \Psr\Log\LoggerInterface $_logger
     * @param OrderPublisher $_publisher
     */

    public function __construct(
        \Psr\Log\LoggerInterface $_logger,
        OrderPublisher $publisher
    ){
        $this->_logger = $_logger;
        $this->_publisher = $publisher;
    }


    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();

        $this->_logger->info('info1234' . time());
        $this->_logger->info( json_encode($order->getData()));


        //$this->sendOrderInfoToGBS();

        $dataTransfer = new DataTransfer();
        $dataTransfer->setCorrelationId('kugerguhejgherg');
        $dataTransfer->setEventEditionCode('nfiehrfiheorifherf');
        $dataTransfer->setOrganisationRxMasterId('rufheoihrfoeirhfoerf');
        $dataTransfer->setPackage('Bronze');

        $this->_publisher->execute($dataTransfer);

        //$this->_logger->debug($order->getData());

       // var_dump($order->getData());
       // exit;
    }


    public function sendOrderInfoToGBS()
    {

        //The URL that we want to send a PUT request to.
        $url = 'https://bb5hgbxnt2.execute-api.eu-west-1.amazonaws.com/api/v1/packages';

//Initiate cURL
        $ch = curl_init($url);

//Use the CURLOPT_PUT option to tell cURL that
//this is a PUT request.
        curl_setopt($ch, CURLOPT_PUT, true);
//        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false)

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

//We want the result / output returned.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//Our fields.
        $fields = [
            'EventEditionCode' => "LBF-20",
            'OrganisationRxMasterId' => "OCDH_OBU_000-ORG-9438495",
            'Package' => "Bronze"
        ];
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));

//Execute the request.
        $response = curl_exec($ch);

        $info = curl_getinfo($ch);

        $this->_logger->info('info from gateway ' . json_encode($info));

        $this->_logger->info('response from gateway ' . $response);

        //echo $response;

//        /*$request = new \Zend\Http\Request();
//      //  $request->setHeaders($httpHeaders);
//        $request->setUri('http://magento.ll/rest/V1/customers/search');
//        $request->setMethod(\Zend\Http\Request::METHOD_PUT);
//
//        $params = new \Zend\Stdlib\Parameters([
//            'EventEditionCode' => "LBF-20",
//            'OrganisationRxMasterId' => "OCDH_OBU_000-ORG-9438495",
//            'Package' => "Bronze"
//        ]);
//
//        $request->setQuery($params);
//
//
//        $client = new \Zend\Http\Client();
//        $options = [
//            'adapter'   => 'Zend\Http\Client\Adapter\Curl',
//            'curloptions' => [CURLOPT_FOLLOWLOCATION => true],
//            'maxredirects' => 0,
//            'timeout' => 30
//        ];
//        $client->setOptions($options);
//
//        $response = $client->send($request);*/
    }
}
